module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    extend: {
      colors: {
        'azure-tide': '#319795',
        'heirloom-hydrangea': '#3182CE',
        'placebo-blue': '#EBF4FF',
        'calm-water': '#E6FFFA',
        'grey-font': '#4A5568',
      },
    },
  },
  plugins: [],
}
